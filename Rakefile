# -*- ruby -*-

nsi_path = "luajit-installer.nsi"
version_major = File.read(nsi_path).scan(/VERSION_MAJOR "(.+?)"/).flatten[0]
version_minor = File.read(nsi_path).scan(/VERSION_MINOR "(.+?)"/).flatten[0]
version_micro = File.read(nsi_path).scan(/VERSION_MICRO "(.+?)"/).flatten[0]
version = [version_major, version_minor, version_micro].join(".")

desc "Tag for #{version}"
task :tag do
  sh("git", "tag", "-a", version, "-m", "#{version} has been released!!!")
  sh("git", "push", "--tags")
end

namespace :version do
  desc "Update version"
  task :update do
    new_version = ENV["VERSION"]
    if new_version.nil?
      raise "Specify new version as VERSION environment variable value"
    end

    new_major, new_minor, new_micro = new_version.split(".")
    nsi_content = File.read(nsi_path)
    nsi_content = nsi_content.gsub(/VERSION_(MAJOR|MINOR|MICRO) ".+?"/) do
      version_type = $1
      case version_type
      when "MAJOR"
        "VERSION_MAJOR \"#{new_major}\""
      when "MINOR"
        "VERSION_MINOR \"#{new_minor}\""
      when "MICRO"
        "VERSION_MICRO \"#{new_micro}\""
      else
        raise "unknown version type: #{version_type}"
      end
    end
    File.write(nsi_path, nsi_content)
  end
end

desc "Build"
task :build do
  sh("makensis", nsi_path)
end
