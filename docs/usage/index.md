---
title: 使い方
---

# 使い方

# Luaスクリプトの実行方法 {#how-to-run-lua-script}

Windowsアプリケーション（コマンドプロンプトやPowerShellターミナルを含
む）からLuaJITを使うためにはいろいろ事前準備が必要なのですが、その準備
をやってくれるランチャープログラムを提供しています。そのランチャープロ
グラムを使えばWindowsアプリケーションから簡単にLuaJITを実行できます。

ランチャープログラムは`C:\luajit\luajit.exe`です。（インストールフォル
ダーを`C:\luajit`から変更した場合は読み替えてください。）

ランチャープログラムにLuaスクリプトのパスを指定するとそのLuaスクリプト
をLuaJITで実行できます。

たとえば、カレントフォルダーに次の内容の`hello.lua`があるとします。

```lua
print("Hello")
```

次のコマンドで`hello.lua`を実行できます。

```console?lang=batchfile
> C:\luajit\luajit.exe hello.lua
Hello
```

次のように`-e`オプションを使えばコマンドラインでLuaスクリプトを指定す
ることもできますが、コマンドプロンプトやPowerShellの文字列の扱いを考慮
しなければいけなくうまくLuaスクリプトを指定できないことがあるのであま
りおすすめしません。

```console?lang=batchfile
> C:\luajit\luajit.exe -e "print([[Hello]])"
Hello
```

使用できるオプションは`luajit.exe`に未知のオプション（たとえば`--help`）
を指定すると確認できます。

```console?lang=batchfile
> C:\luajit\luajit.exe --help
usage: C:\luajit\msys64\mingw64\bin\luajit.exe [options]... [script [args]...].
Available options are:
  -e chunk  Execute string 'chunk'.
  -l name   Require library 'name'.
  -b ...    Save or list bytecode.
  -j cmd    Perform LuaJIT control command.
  -O[opt]   Control LuaJIT optimizations.
  -i        Enter interactive mode after executing 'script'.
  -v        Show version information.
  -E        Ignore environment variables.
  --        Stop handling options.
  -         Execute stdin and stop handling options.
```

## Luaモジュールのインストール方法 {#how-to-install-lua-module}

Windows LuaJIT Installerは[LuaRocks][luarocks]もインストールするので
LuaRocksでLuaモジュールをインストールできます。

Windowsアプリケーション（コマンドプロンプトやPowerShellターミナルを含
む）からLuaRocksを使うためにはいろいろ事前準備が必要なのですが、その準
備をやってくれるランチャープログラムを提供しています。そのランチャープ
ログラムを使えばWindowsアプリケーションから簡単にLuaRokcsを使ってLuaモ
ジュールをインストールできます。

ランチャープログラムは`C:\luajit\luarocks.exe`です。（インストールフォ
ルダーを`C:\luajit`から変更した場合は読み替えてください。）

このランチャープログラムを使って[luautf8][luautf8]をインストールするに
は次のようにします。

```batchfile
C:\luajit\luarocks.exe install luautf8
```

`luarocks`コマンドの使い方は`C:\luajit\luarocks --help`または[LuaRocks
のドキュメント][luarocks-document]を参照してください。


たとえば、アンインストールする場合は次のようにします。

```batchfile
C:\luajit\luarocks.exe remove luautf8
```

たとえば、検索する場合は次のようにします。

```batchfile
C:\luajit\luarocks.exe search luautf8
```

既存のCライブラリーを使用するLuaモジュールをインストールする場合は、事
前に対象のCライブラリーをインストールする必要があります。たとえば、
[XMLua][xmlua]をインストールする場合は[libxml2][libxml2]が必要です。

Windwos LuaJIT InstallerはMSYS2をベースにしているのでMSYS2のパッケージ
管理機能を使って簡単に既存のCライブラリーをインストールできます。

MSYS2のパッケージ管理ツールは[pacman][pacman]です。もちろん、
`C:\luajit\pacman.exe`というランチャープログラムを用意しているので
Windowsアプリケーションからも簡単に使えます。

MSYS2のパッケージはコマンドラインでも[MSYS2のパッケージ検索サービ
ス][msys2-package-search]（「Search in」は「Base Packages」ではなく
「Packages」を指定してください）でも検索できます。コマンドラインで検索
する場合は次のように`--sync --search`オプションを使います。

```console?lang=batchfile
> C:\luajit\pacman.exe --sync --search libxml2
mingw32/mingw-w64-i686-gxml 0.18.1-1
    GObject Libxml2 wrapper and Serializer Framework (mingw-w64)
mingw32/mingw-w64-i686-libxml++ 3.2.0-1
    C++ wrapper for the libxml2 XML parser library (mingw-w64)
mingw32/mingw-w64-i686-libxml++2.6 2.40.1-1
    C++ wrapper for the libxml2 XML parser library (mingw-w64)
mingw32/mingw-w64-i686-libxml2 2.9.10-4
    XML parsing library, version 2 (mingw-w64)
mingw32/mingw-w64-i686-python-lxml 4.5.0-1
    Python binding for the libxml2 and libxslt libraries (mingw-w64)
mingw32/mingw-w64-i686-xmlsec 1.2.30-1
    XML Security Library is a C library based on LibXML2 (mingw-w64)
mingw64/mingw-w64-x86_64-gxml 0.18.1-1
    GObject Libxml2 wrapper and Serializer Framework (mingw-w64)
mingw64/mingw-w64-x86_64-libxml++ 3.2.0-1
    C++ wrapper for the libxml2 XML parser library (mingw-w64)
mingw64/mingw-w64-x86_64-libxml++2.6 2.40.1-1
    C++ wrapper for the libxml2 XML parser library (mingw-w64)
mingw64/mingw-w64-x86_64-libxml2 2.9.10-4
    XML parsing library, version 2 (mingw-w64)
mingw64/mingw-w64-x86_64-python-lxml 4.5.0-1
    Python binding for the libxml2 and libxslt libraries (mingw-w64)
mingw64/mingw-w64-x86_64-xmlsec 1.2.30-1
    XML Security Library is a C library based on LibXML2 (mingw-w64)
msys/libxml2 2.9.10-5 (libraries)
    XML parsing library, version 2
msys/libxml2-devel 2.9.10-5 (development)
    Libxml2 headers and libraries
msys/libxml2-python 2.9.10-5 (python-modules)
    Libxml2 python modules
msys/perl-Alien-Libxml2 0.16-1 (perl-modules)
    Install the C libxml2 library on your system
```

たくさんあって迷いますが注目するポイントは`mingw-x64-x86_64-`がついて
いるかどうかです。`mingw-x64-x86_64-`がついているかと（パッケージの説
明ではなく）パッケージ名に`libxml2`が含まれているかどうかで絞り込むと
`mingw-x64-x86_64-libxml2`だけが残ります。

参考：`mingw-x64-x86_64-`がついているパッケージは（MSYS2用ではなく）
Windows用の64ビットのパッケージということを示しています。Windows
LuaJIT Installerは（MSYS2用ではなく）Windows用の64bitのLuaJIT環境を提
供しているので同じ環境用のCライブラリーのパッケージを使う必要がありま
す。

パッケージ名がわかったら`--sync --noconfirm ${パッケージ名}`でインストー
ルできます。たとえば、libxml2のパッケージは次のようにインストールしま
す。

```batchfile
C:\luajit\pacman.exe --sync --noconfirm mingw-w64-x86_64-libxml2
```

不要になったパッケージは`--remove --recursive ${パッケージ}`でアンイン
ストールできます。たとえば、libxml2のパッケージは次のようにアンインス
トールします。

```batchfile
C:\luajit\pacman.exe --remove --recursive mingw-w64-x86_64-libxml2
```

## 上級者向けの使い方 {#advanced-user}

Windows LuaJIT Installerは[MSYS2][msys2]をベースにLuaJIT環境を構築しま
す。MSYS2内の[Bash][bash]ターミナルを起動するとMSYS2内で作業できます。
LinuxやmacOSのターミナルなどUNIX系のターミナルの操作になれている場合は
Bashターミナルで作業した方が便利です。ランチャープログラムは
`luajit.exe`や`luarocks.exe`などよく使うコマンドのものだけしか用意して
いませんが、MSYS2内ではすべてのコマンドを使えます。

Bashターミナルを起動するにはスタートメニュー内の「Bash」というショート
カットを使います。

たとえば、Bashターミナル内で[XMLua][xmlua]をインストールするには次のコ
マンドを実行します。

```bash
pacman --sync --needed --noconfirm mingw-w64-x86_64-libxml2
luarocks install xmlua
```

MSYS2内にはUNIX系の便利なコマンドがたくさんあるので、次のように
`grep`を使って結果をフィルターすることも簡単です。

```console?lang=bash
$ luarocks --help | grep -A1 search
	search
		Query the LuaRocks servers.
```

Bashターミナルは`exit`で抜けることができます。

[luarocks]:https://luarocks.org/

[msys2]:https://www.msys2.org/

[bash]:https://www.gnu.org/software/bash/

[luautf8]:https://luarocks.org/modules/xavier-wang/luautf8

[luarocks-document]:https://github.com/luarocks/luarocks/wiki/luarocks

[xmlua]:https://clear-code.github.io/xmlua/

[libxml2]:http://xmlsoft.org/

[pacman]:https://wiki.archlinux.org/index.php/pacman

[msys2-package-search]:https://packages.msys2.org/search?t=binpkg

[libxml2-package]:https://packages.msys2.org/base/mingw-w64-libxml2

[winpty]:https://github.com/rprichard/winpty
